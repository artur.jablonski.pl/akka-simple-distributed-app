/*
 * Copyright (c) 2019 Artur Jabłoński
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tech.voee;

import akka.actor.ActorSystem;
import akka.cluster.Cluster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App
{
  private static final Logger log = LoggerFactory.getLogger(App.class);

  public final static String COORDINATOR_CLUSTER_ROLE =
    "CoordinatorClusterRole";

  public static void main(String[] args)
  {
    // Create an Akka system
    ActorSystem system = ActorSystem.create("ClusterSystem");
    // load Cluster extension
    Cluster cluster = Cluster.get(system);

    if (cluster.selfRoles().contains(COORDINATOR_CLUSTER_ROLE)) {
      cluster.registerOnMemberUp(
        () -> {
          log.info(
            "This is coordinator node... starting the coordinator");

          if (args.length < 1)
            throw new IllegalStateException(
              "You need to pass data file path as first argument");

          system
            .actorOf(CoordinatorActor.props(args[0]), CoordinatorActor.NAME);
        }
      );
    } else {
      cluster.registerOnMemberUp(
        () -> {
          log.info(
            "This is worker node... starting the workers");
          for (int i = 0; i < Runtime.getRuntime().availableProcessors(); i++)
            system.actorOf(WorkerActor.props(), WorkerActor.NAME + i);

        }
      );
    }

  }
}
