/*
 * Copyright (c) 2019 Artur Jabłoński
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tech.voee;

import akka.NotUsed;
import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.DeadLetter;
import akka.actor.Props;
import akka.cluster.pubsub.DistributedPubSub;
import akka.cluster.pubsub.DistributedPubSubMediator;
import akka.japi.Pair;
import akka.pattern.Patterns;
import akka.stream.ActorMaterializer;
import akka.stream.SourceRef;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;

import java.time.Duration;
import java.util.concurrent.CompletionStage;

public class WorkerActor extends AbstractLoggingActor
{
  public final static String NAME = "worker";
  private final ActorRef mediator =
    DistributedPubSub.get(getContext().system()).mediator();
  private final ActorMaterializer materializer =
    ActorMaterializer.create(getContext().getSystem());

  public static Props props()
  {
    return Props.create(WorkerActor.class);
  }

  // subscribe to cluster changes
  @Override
  public void preStart()
  {
    getContext().getSystem()
                .getEventStream()
                .subscribe(getSelf(), DeadLetter.class);

    sendWorkerReadyViaMediator();
  }

  private void sendWorkerReadyViaMediator()
  {
    mediator
      .tell(new DistributedPubSubMediator.Send("/user/" + CoordinatorActor.NAME,
                                               new CoordinatorActor.WorkerReady()),
            getSelf());

  }

  @Override
  public Receive createReceive()
  {
    return receiveBuilder()
             .match(SourceRef.class,
                    sourceRef -> {
                      getContext().getSystem()
                                  .getEventStream()
                                  .unsubscribe(getSelf(), DeadLetter.class);

                      log().info("SourceRef received, commencing streaming");

                      //noinspection unchecked
                      CompletionStage<CoordinatorActor.PartialResult>
                        partialResultFuture =
                        ((Source<Integer, NotUsed>) sourceRef.getSource())
                          .zipWithIndex()
                          .fold(
                            Pair.create(0, 0L),
                            (acc, curr) ->
                              Pair.create(acc.first() + curr.first(),
                                          curr.second() + 1)
                          )
                          .map(
                            p -> new CoordinatorActor.PartialResult(
                              p.second() == 0 ?
                              0 :
                              p.first().doubleValue() / p.second(),
                              p.second())
                          )
                          .runWith(Sink.head(), materializer);

                      Patterns.pipe(partialResultFuture,
                                    context().dispatcher())
                              .to(sender());
                    }
             )
             .match(DeadLetter.class,
                    dl -> {
                      if (dl.sender().equals(getSelf()) &&
                          dl.message() instanceof CoordinatorActor.WorkerReady) {
                        log().info(
                          "Dead letter detected to be caused by PubSub gossip delay, reattempting scheduled");
                        getContext()
                          .getSystem()
                          .scheduler()
                          .scheduleOnce(
                            Duration.ofMillis(100),
                            this::sendWorkerReadyViaMediator,
                            getContext().getSystem().dispatcher());
                      }
                    }
             )
             .matchAny(o -> log().info(
               "Unexpected message received: {}",
               o))
             .build();
  }
}