/*
 * Copyright (c) 2019 Artur Jabłoński
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tech.voee;

import akka.NotUsed;
import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.cluster.pubsub.DistributedPubSub;
import akka.cluster.pubsub.DistributedPubSubMediator;
import akka.pattern.Patterns;
import akka.stream.ActorMaterializer;
import akka.stream.SourceRef;
import akka.stream.javadsl.FileIO;
import akka.stream.javadsl.Framing;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.PartitionHub;
import akka.stream.javadsl.Source;
import akka.stream.javadsl.StreamRefs;
import akka.util.ByteString;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.File;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.concurrent.CompletionStage;

public class CoordinatorActor extends AbstractLoggingActor
{
  public static final String NAME = "coordinator";
  private final ActorMaterializer materializer =
    ActorMaterializer.create(getContext().getSystem());
  private Source<Integer, NotUsed> partitionHubSource;
  private String dataFilePath;

  private Integer numOfWorkers = 0;
  private PartialResult partialResult = new PartialResult(0.0, 0L);

  public CoordinatorActor(String dataFilePath)
  {
    this.dataFilePath = dataFilePath;
  }

  public static Props props(String path)
  {
    return Props
             .create(CoordinatorActor.class, () -> new CoordinatorActor(path));
  }

  @Override
  public void preStart()
  {
    ActorRef mediator = DistributedPubSub.get(getContext().system()).mediator();
    mediator.tell(new DistributedPubSubMediator.Put(getSelf()), getSelf());
    log().info("coordinator registered with pubsub, {}", getSelf().path());

    //creating our stream with PartitionHub plugged in
    this.partitionHubSource = prepareSource(dataFilePath);

  }

  private Source<Integer, NotUsed> prepareSource(String path)
  {
    return
      FileIO.fromFile(new File(path))
            .via(Framing.delimiter(ByteString.fromString("\n"), 1024))
            .map(b -> b.decodeString(StandardCharsets.UTF_8))
            .map(Integer::parseInt)
            .toMat(
              PartitionHub
                .of(Integer.class,
                    (size, elem) -> Math.abs(elem.hashCode() % size), 1, 256),
              Keep.right())
            .run(materializer);
  }

  @Override
  public Receive createReceive()
  {
    return receiveBuilder()
             .match(WorkerReady.class,
                    w -> {
                      log().info("+1 worker");
                      numOfWorkers++;

                      CompletionStage<SourceRef<Integer>> sourceRefFuture =
                        partitionHubSource
                          .runWith(StreamRefs.sourceRef(), materializer);

                      Patterns.pipe(sourceRefFuture,
                                    context().dispatcher())
                              .to(sender(), getSelf());
                    }
             )
             .match(PartialResult.class,
                    p -> {
                      log().info("-1 worker");
                      partialResult = partialResult.combine(p);
                      if (--numOfWorkers == 0)
                        log().info("The final result is: {}", partialResult);
                    })
             .matchAny(o ->
                         log().info("Unexpected message received: {}", o))
             .build();
  }

  //protocol
  public static class WorkerReady implements Serializable {}

  @Data
  @AllArgsConstructor
  public static class PartialResult implements Serializable
  {
    private Double mean;
    private Long count;

    public PartialResult combine(PartialResult other)
    {
      if (other == null || other.getCount() == 0)
        return this;

      if (count == 0)
        return other;

      return new PartialResult(
        (this.getMean() * this.getCount() +
         other.getMean() * other.getCount()) /
        (this.getCount() + other.getCount()),
        this.getCount() + other.getCount()
      );
    }
  }
}