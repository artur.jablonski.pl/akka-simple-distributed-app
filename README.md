# A simple distributed application with _Akka_ toolkit (v2.5.26) and _Java_ (v1.8).

## Overview

In this post I will go over an implementation of a simple
distributed application using _Akka_ toolkit and _Java_.
I will start off with a real world use case of a "manually
distributed" process and I will introduce some of _Akka_ and other
tools to automate it as much as possible.
Hopefully this read will give you a good idea what the
various tools are, what they are for and how to use them
in a concrete example of a distributed process.

Some familiarity with _Akka actor systems_ is assumed though you can still follow
the post to great extent without that knowledge. For a quick read on _Akka actors_
please check [this post](https://www.baeldung.com/akka-actors-java) and for a deeper dive check _Akka's_ documentation
[here](https://doc.akka.io/docs/akka/2.5/guide/index.html) that is available
for both _Java_ and _Scala_.

All the code is available in git repo [here](https://gitlab.com/artur.jablonski.pl/akka-simple-distributed-app)

## Real world use case of manually distributed process

This post has been inspired by a conversation I had with a friend
who works as a data scientist.
He told me about how he wrote an algorithm to crunch through
a relatively large (GBs) _CSV_ data file and how it would take "forever"
to get results. He couldn't see any obvious inefficiencies in his
implementation and it looked as if the poor performance was due to
the data volume alone.

He figured out that if he could split the data and let his alg run
simultaneously on multiple machines and then combine the partial results,
this would significantly speed up the process. Good thinking. 
He happened
to have access to a computer lab with a number of machines available for him to use.
So what he did was, he manually split the big data file into smaller chunks,
he then copied the files onto a usb drive together with executable of his
algorithm that could take a data file as a parameter
and then he went with the usb drive from one lab machine to another 
starting his program with different input files for each instance he launched.
He then waited for all the partial results, copied them back on the drive
and combined them somewhat manually to arrive at the final result.

This approach worked and he got his results way faster
even if there was a lot of manual work involved (and a lot of opportunities
for introducing an error as a result). For a one-off calculation
this probably was OK, but for more frequent calculations, running 
around a room with a usb drive is probably not the best approach.

Let's see how we can improve it. 

## What will we build. 

For simplicity let's assume that instead of a _CSV_ file we have a
file with one integer number per row. Our task 
is to calculate arithmetic mean of all the numbers in the file. We want to 
be able to use any number of nodes to do this and then combine the partial results
in automatic way to produce final result. 

## Akka Cluster

Before we go and implement anything, it's important to understand
the concept of _Akka Cluster_, which will be part of the solution here. 
_Akka Cluster_ is the underlying technology that makes multiple independent
instances of _Akka Cluster nodes_ to appear as one logical unit on which
a distributed application runs. As simple as it sounds, the 
implementation is far from trivial and involves a gossip protocol that
all the nodes use to have consistent view of the cluster state even when
facing node crashes, network partitions, etc. The documentation of
[_Akka Cluster_](https://doc.akka.io/docs/akka/2.5/index-cluster.html)
 is very detailed and I highly recommend going through it.

In a way, when my data scientist friend was in the lab and chose the machines that
he would then use to perform parts of the task, he formed a cluster
of machines. Because he himself was the communication channel that would orchestrate 
the task, the computers themselves didn't need to know about each other at all. In
case of _Akka Cluster_ however, the nodes involved need to be able to talk to each
other via network in a peer to peer fashion. 

One crucial aspect of _Akka Cluster_ is how a node joins a cluster. 
Imagine you are starting an _Akka Cluster node_. How does it find 
the other nodes of the cluster it is supposed to become a member of?
There are various ways this can happen and the choice is in a way
dictated by the infrastructure the _Akka Cluster_ will be deployed to.

One way of discovering who to talk to to join a cluster is via statically defined
seed nodes. Those are preconfigured first points of contact for any 
node that wants to join.

This is the relevant excerpt from `application.conf` (_Akka_'s configuration file):

```
cluster {
    seed-nodes = [
      "akka.tcp://ClusterSystem@127.0.0.1:2551",
      "akka.tcp://ClusterSystem@127.0.0.1:2552"]
}
```

So what we're saying to any cluster node that starts using this configuration
is that the primary point of contact to join the cluster is a remote (akka.tcp://)
_Akka actor system_ named _ClusterSystem_ that can be reached on IP address `127.0.0.1` on port `2551`
and the secondary point of contact is an _Akka actor system_ with the same name running on port `2552` on the same host.

**NOTE**: 
You may be surprised that nodes of _Akka Cluster_ can run on the same
host, but a node here is "just" _Akka actor system_, so you can run many of those
on the same host (or even in the same _JVM_ process!) and they would still
form a single _Akka Cluster_. 
That said, if the reason you are building distributed application is to
use combined computational power of several machines, then of course it 
only makes sense that the nodes run on different machines.

This cluster joining scheme can work well on a LAN network, where machines 
normally have static IP addresses (ie they don't change often or at all). 
What can make this approach a bit more flexible is that the
seed nodes configuration as well as listening port configuration
don't need to be hardcoded in the `application.conf`
file, but can be passed when launching a node as a system property instead.

So imagine we have our solution packaged in `solution.jar` file with 
_Main-Class_ header set in the jar's _manifest file_ and imagine we want
to run it over a cluster of four _Akka Cluster nodes_ each running on its own
host on a local LAN on machines with IP addresses from `192.168.1.1` to
`192.168.1.4` with the `192.168.1.1` machine being the only seed node. We could start all the processes like this:

On host: `192.168.1.1`
```
$> java -Dakka.remote.netty.tcp.hostname=192.168.1.1 \
        -Dakka.remote.netty.tcp.port=2551 \
        -Dakka.cluster.seed-nodes.0=akka.tcp://ClusterSystem@192.168.1.1:2551 \
        -jar solution.jar
```

On host: `192.168.1.2`

```
$> java -Dakka.remote.netty.tcp.hostname=192.168.1.2 \
        -Dakka.remote.netty.tcp.port=2551 \
        -Dakka.cluster.seed-nodes.0=akka.tcp://ClusterSystem@192.168.1.1:2551 \
        -jar solution.jar
```

On host: `192.168.1.3`

```
$> java -Dakka.remote.netty.tcp.hostname=192.168.1.3 \
        -Dakka.remote.netty.tcp.port=2551 \
        -Dakka.cluster.seed-nodes.0=akka.tcp://ClusterSystem@192.168.1.1:2551 \
        -jar solution.jar
```

On host: `192.168.1.4`
```
$> java -Dakka.remote.netty.tcp.hostname=192.168.1.4 \
        -Dakka.remote.netty.tcp.port=2551 \
        -Dakka.cluster.seed-nodes.0=akka.tcp://ClusterSystem@192.168.1.1:2551 \
        -jar solution.jar
```

Since we are running each process on different host, we can use the same
port `2551` on all instances and always use the `192.168.1.1` address as the seed node.
We could also use port `0` for all nodes but the seed node so that a random 
port would be assigned by the operating system. 
 
This could perhaps be little improved by using a simple script to get us the IP
of the machine the script runs on and therefore avoid hardcoding it in the invocation
line like above, but the bottom line is that the IP addresses would have to be resolved
before each node is started. 

There are other ways of bootstrapping _Akka Cluster_ that are more dynamic and
I will explore them in a follow up post. For now 
let's keep it simple and go with the approach above.

## The implementation outline

So now that we have our _Akka Cluster_, let's implement the solution. 

There will be two type of actors involved. `CoordinatorActor` and `WorkerActor`. 
There will be only one instance of `CoordinatorActor` and multiple instances of 
`WorkerActors` running in the cluster. 

`CoordinatorActor` responsibilities are:
- partition input data across all available workers
- collect partial results
- produce the final result

`WorkerActor` responsibilities are:
- calculate arithmetic mean across all numbers sent by `CoordinatorActor`
- send the result back together with the count of the numbers to `CoordinatorActor`

_Akka Cluster_ nodes can have roles assigned to them. We'll use this feature
to designate one node as a "coordinator" which will start the `CoordinatorActor`
actor. All other nodes in the cluster will start `WorkerActor` actors that will perform
the calculation.

To assign a role to a cluster node we can start the _Java_ process with appropriate
property set. In our case it makes sense to run our `CoordinatorActor` on the
first seed node. Building form the previous example, this can be done like this:

```
$> java -Dakka.remote.netty.tcp.hostname=192.168.1.1 \
        -Dakka.remote.netty.tcp.port=2551 \ 
        -Dakka.cluster.seed-nodes.0=akka.tcp://ClusterSystem@192.168.1.1:2551 \
        -Dakka.cluster.roles.0=CoordinatorClusterRole \
        -jar solution.jar
```

Now, in our code we can detect if the role is assigned to the node and
start the appropriate actors accordingly:

```java
if (cluster.selfRoles().contains(COORDINATOR_CLUSTER_ROLE)) {
  cluster.registerOnMemberUp(
    () -> {
      log.info(
        "This is coordinator node... starting the coordinator");

      system.actorOf(CoordinatorActor.props(), "coordinator");
    }
  );
} else {
  cluster.registerOnMemberUp(
    () -> {
      log.info(
        "This is worker node... starting the worker");

      system.actorOf(WorkerActor.props(), "worker");

    }
  );
}
```

Here the actor creating code will only be triggered when the cluster
node has fully joined the cluster (hooked to the lifecycle via the
`registerOnMemberUp` method).

Our worker nodes may be running on multicore processor hosts, so it
would make sense to utilize all cores and start as many workers as
available cores. Let's make that adjustment:

```java
cluster.registerOnMemberUp(
        () -> {
          log.info(
            "This is worker node... starting the workers");
          for (int i = 0; i < Runtime.getRuntime().availableProcessors(); i++)
            system.actorOf(WorkerActor.props(), "worker" + i);

        }
);
``` 

So now we need to make the `WorkerActor`s and `CoordinatorActor` talk to each other. 
The way this will work is that each `WorkerActor` will introduce himself
to the `CoordinatorActor` by sending `WorkerReady` message upon which the 
`CoordinatorActor` will commence streaming the numbers to it. But how can 
a `WorkerActor` know where the `CoordinatorActor` is in the cluster? For now we assumed
that it runs on the first seed node of the cluster, and we could use 
that knowledge to lookup the `CoordinatorActor` by its network address, 
but that would be a bit brittle, so let's use a different approach 
using [_Distributed PubSub_](https://doc.akka.io/docs/akka/2.5/distributed-pub-sub.html)
mechanism which is an _Akka Cluster_ extension available.

Basically the `CoordinatorActor` upon start will register itself in the
`DistributedPubSubMediator` actor:

```java
ActorRef mediator = DistributedPubSub.get(getContext().system()).mediator();
mediator.tell(new DistributedPubSubMediator.Put(getSelf()), getSelf());    
```

And then the `WorkerActor` can look it up and send a message to it without
knowing upfront where it actually is running:

```java
ActorRef mediator = DistributedPubSub.get(getContext().system()).mediator();
mediator.tell(new DistributedPubSubMediator.Send("/user/coordinator",
                                               new CoordinatorActor.WorkerReady()),
            getSelf());
```

This almost works... the problem being that the way the _Distributed Pub Sub_ mechanism
works is that on each cluster node it runs a _mediator actor_. All the _mediator actors_
across all nodes gossip with each other about registered actors that can be reached.

So on our `WorkerActor` node, this is the line where the _mediator actor_ is started:

```java
ActorRef mediator = DistributedPubSub.get(getContext().system()).mediator();
```

And the next line of code that immediately follows:

```java
mediator.tell(new DistributedPubSubMediator.Send("/user/coordinator",
                                               new CoordinatorActor.WorkerReady()),
            getSelf());

```

tries to lookup and send a message to the `CoordinatorActor`, but
even if the `CoordinatorActor` is already registered in the corresponding _mediator actor_
on its own node, there is simply not enough time for the gossip about this to reach
the _mediator actor_ in the `WorkerActor` node. We could work around this in a simple way
by delaying sending the `WorkerReady` message by say a second using _Scheduler API_ available:

```java
getContext()
      .getSystem()
      .scheduler()
      .scheduleOnce(
        Duration.ofMillis(1000),
        () -> mediator
                .tell(new DistributedPubSubMediator.Send("/user/coordinator",
                                                         new CoordinatorActor.WorkerReady()),
                      getSelf()),
        getContext().getSystem().dispatcher());
```

But it's as brittle as it is tempting. Let's do something better. Unfortunately there seems
not to be any API on the mediator itself to poll for availability of an actor
under specific address in the mediator registry, but if we try to send a message to an 
actor via the mediator and that actor is not there, the message will end up in so called
_dead letter queue_. We can subscribe to _dead letter events_ and upon receiving one that
was caused by our `CoordinatorActor` not being available in the registry yet, we can
schedule another attempt at sending it later. This is how it looks in the code:

```java
getContext().getSystem()
                .getEventStream()
                .subscribe(getSelf(), DeadLetter.class);

mediator.tell(new DistributedPubSubMediator.Send("/user/coordinator",
                                                     new CoordinatorActor.WorkerReady()),
                  getSelf());
``` 
Now that we subscribed to `DeadLetter` messages we can attempt to send `WorkerReady` message 
straight away and then in our message handing code we can capture `DeadLetter` messages
and schedule next attempt:

```java
.match(DeadLetter.class,
                    dl -> {
                      if (dl.sender().equals(getSelf()) &&
                          dl.message() instanceof CoordinatorActor.WorkerReady) {
                        log.info(
                          "Dead letter detected to be caused by PubSub gossip delay, reattempting scheduled");
                        getContext()
                          .getSystem()
                          .scheduler()
                          .scheduleOnce(
                            Duration.ofMillis(100),
                            () -> mediator
                                    .tell(new DistributedPubSubMediator.Send(
                                            "/user/coordinator",
                                            new CoordinatorActor.WorkerReady()),
                                          getSelf()),
                            getContext().getSystem().dispatcher());
                      }
                    }
)
``` 

Neat! Now we have more reliable and flexible mechanism in place that can come
handy if we choose not to run the `CoordinatorActor` on the first node that starts. 

So now we have the `WorkerActor`s introducing themselves to the `coordinatorActor` correctly, 
we can start sending the numbers over to them and for that we'll use _Akka Streams_.

[_Akka Streams_](https://doc.akka.io/docs/akka/2.5/stream/index.html) is an implementation 
of _reactive streams_ specification. _Reactive streams_ is a pretty big topic in itself
and way beyond the scope of this post. There are tonnes of materials out there on 
the subject. The _Akka Streams_ documentation is a great place to start and 
I wrote an article comparing various reactive streams implementations
available for _Java_ which can serve as introduction to _Akka Streams_ especially
if you are familiar with _RxJava_ or _Project Reactor_. You can read it 
[here](https://gitlab.com/artur.jablonski.pl/akka_streams_vs_rx_reactor).

So why are _Akka Streams_ a good choice here? First, because it has a *flow control*
built-in, second because it can *span network boundaries* and third, because
it can handle *partitioning data to dynamic number of destinations*. So let's go in 
turn and talk about those in turn. 

The flow control here means that a `WorkerActor` can slow down the `CoordinatorActor` 
if it can't keep up with the amount of data it's being passed. If we chose to
send the data over to `WorkerActor`s by means of the `Actor`'s `tell` method, we could
overflow its incoming messages mailbox as there's no flow control in place, of course
nothing stops us from implementing such flow control over the actor mailboxes,
but why would we if there's _Akka Streams_ for us to use.

The second part is the fact that _Akka Streams_ can work across network. 
Here this is a hard requirement, we are building a distributed system, of course
it needs to communicate over the network! There's one aspect of this across-network operation
that is worth 
mentioning and it has to do with delivery guarantees of messages in `Akka`. 
The basic guarantee in `Akka` is *at-most-once*, which means that message
sent from one actor to another can be lost on a way and you wouldn't know
that it happened. As bad as it sounds, there are cases when this message
delivery guarantee is enough. Take the `WorkerReady` message sent from `WorkerActor`
to `CoordinatorActor` as an example. This message can be lost and the consequences
of it will be that the `WorkerActor` will wait for data coming and never get any. 
On an assumption that such message loss is quite rare and that the application
creates multiple `WorkerActor` actors I am OK that sometimes, on some runs, 
some workers will just be doing nothing. If I decided it was unacceptable, I 
would need to implement stronger guarantees on top of the basic one
(which basically means introducing ACKs, redeliveries with timeouts on the
sending side and duplication detection on the receiving side). 
The data that we send over from the `CoordinatorActor` to `WorkerActor`s travels
over network and as such is just as any other message a subject to delivery
guarantees. I am, as a designer of the system, definitely not OK about
some data item getting lost on the way without the system noticing it 
and happily producing some result that is simply wrong. Had I chose to 
send the data items over the `tell` method I would not only need to worry
about overflowing the receiving actor but also that the data can go missing
in transit. _Akka Streams_ when going over network does not guarantee that
all data items will be delivered successfully, however it guarantees that 
if it happens, this will be **detected and failure will be raised**.

The third good reason is that _Akka Streams_ comes with `PartitionHub` implementation
which can be used to partition data to dynamically changing number of 
destinations, which is exactly what we want here. As the `WorkerActor`s are
being created and report their readiness they are plugged in to live stream of 
data on the go. `PartitionHub` implementation guarantees that no two receivers 
will ever get the same data item. Awesome.

So after that long digression let's jump back to the code. Let's simplify 
things a little bit and let's forget that our data lives in some file somewhere
on the filesystem, but instead let's just generate a stream of integers from `0` to `1000`
in-memory. This will happen during `CoordinatorActor` startup. The stream
will be prepared, hooked to `PartitionHub` and ready for the first eager `WorkerActor`
to knock at the door:

```java
private Source<Integer, NotUsed> prepareSource()
{
  return
    Source.range(0, 1000)
          .toMat(
            PartitionHub
              .of(Integer.class,
                  (size, elem) -> Math.abs(elem.hashCode() % size), 1, 256),
            Keep.right())
          .run(materializer);
}
```

Once that happens, `the CoordinatorActor` will create a `SourceRef` which
it will then send back to the `WorkerActor` as a response to `WorkerReady` message:

```java
.match(WorkerReady.class,
                   w -> {
                     log.info("WorkerReady received");
                     CompletionStage<SourceRef<Integer>> sourceRefFuture =
                       partitionHubSource
                         .runWith(StreamRefs.sourceRef(), materializer);

                     Patterns.pipe(sourceRefFuture,
                                   context().dispatcher())
                             .to(sender(), getSelf());
                   }
)
```

That `SourceRef`, can be then used by the `WorkerActor` to start receiving data
and perform calculations and send it back to the coordinator once
the streaming is completed. This is how this happens in the code
that is handling `SourceRef` message in the `WorkerActor`:

```java
.match(SourceRef.class,
                    sourceRef -> {

                      log().info("SourceRef received, commencing streaming");

                      //noinspection unchecked
                      CompletionStage<CoordinatorActor.PartialResult>
                        partialResultFuture =
                        ((Source<Integer, NotUsed>) sourceRef.getSource())
                          .zipWithIndex()
                          .fold(
                            Pair.create(0, 0L),
                            (acc, curr) -> Pair
                                             .create(acc.first() + curr.first(),
                                                     curr.second() + 1)
                          )
                          .map(
                            p -> new CoordinatorActor.PartialResult(
                              p.second() == 0 ?
                              0 :
                              p.first().doubleValue() / p.second(),
                              p.second())
                          )
                          .runWith(Sink.head(), materializer);

                      Patterns.pipe(partialResultFuture,
                                    context().dispatcher())
                              .to(sender());
                    }
)
```

Upon receiving the `SourceRef`, the `WorkerActor` uses `zipWithIndex` to keep track
of the count of items going through the stream. Then it runs a `fold` operation
to sum the elements and keep the total count. Finally, in `map` a `PartialResult`
object is instantiated that will contain the arithmetic mean and total count
of items over which the mean was calculated. That object is then piped back
to the `CoordinatorActor`. 

So the last remaining bit is for the `CoordinatorActor` to keep track and 
combine all the `PartialResult` objects being sent back by the `WorkerActors`.
For that, the `CoordinatorActor` will keep track of the number of `WorkerActors`
that sent `WorkerReady` message to it, so that it knows how many `PartialResult`
objects it should expect. It uses `PartialResult.combine()` method to combine
together coming responses. Here are the relevant bits of the code:

```java
.match(WorkerReady.class,
                   w -> {
                     log.info("+1 worker");
                     numOfWorkers++;
                     ...
                   })
.match(PartialResult.class,
                   p -> {
                     log.info("-1 worker");
                     partialResult = partialResult.combine(p);
                     if (--numOfWorkers == 0)
                       log.info("The final result is: {}", partialResult);
                   })

public static class PartialResult implements Serializable
{
  private Double mean;
  private Long count;

  public PartialResult combine(PartialResult other)
  {
    if (count == 0)
      return other;

    if (other.getCount() == 0)
      return this;

    return new PartialResult(
      (this.getMean() * this.getCount() +
       other.getMean() * other.getCount()) /
      (this.getCount() + other.getCount()),
      this.getCount() + other.getCount()
    );
  }
}
```

Upon receiving the last `PartialResult` expected, the `CoordinatorActor`
will simply print out the final result to its standard output. 

What we are missing now is to take the data from a file as opposed
to generating them in memory as we do now. Let's assume that when
starting the "coordinator" node we will pass an argument to the
program which will be a path to the data file:

```
$> java -Dakka.remote.netty.tcp.hostname=192.168.1.1 \
        -Dakka.remote.netty.tcp.port=2551 \ 
        -Dakka.cluster.seed-nodes.0=akka.tcp://ClusterSystem@192.168.1.1:2551 \
        -Dakka.cluster.roles.0=CoordinatorClusterRole \
        -jar solution.jar /path/to/the/dataFile
        
```

We will need to adjust the stream prepared during the `CoordinatorActor` startup:

```java
 private Source<Integer, NotUsed> prepareSource(String path)
  {
    return
      FileIO.fromFile(new File(path))
            .via(Framing.delimiter(ByteString.fromString("\n"), 1024))
            .map(b -> b.decodeString(StandardCharsets.UTF_8))
            .map(Integer::parseInt)
            .toMat(
              PartitionHub
                .of(Integer.class,
                    (size, elem) -> Math.abs(elem.hashCode() % size), 1, 256),
              Keep.right())
            .run(materializer);
  }
```

We are using _Akka Stream_ built in capabilities to stream bytes from filesystem
and then using `Framing` for splitting the bytes on new line characters. We
then parse the strings as `Integers`. Everything else stays the same. 
We'll need to pass the path from the program arguments to the `CoordinatorActor`
and we are good to go. Of course we are assuming that the data file is fine and
actually has one `Integer` number per line, anything else will blow up at
the Integer parsing stage. 

## Summary

In this post we went from a "manually distributed" process to one that is way more automated.
We introduced _Akka Cluster_ as an underlying technology to group multiple
_Akka actor systems_ potentially running on different physical machines
into logical unit on which we can implement distributed solution.
We then introduced _Distributed PubSub_ as a way for actors in _Akka Cluster_
to communicate without knowing on which node they are running. We then discussed
_Akka Streams_ as a good fit for streaming large volumes of data between actors due
to its built-in flow control, cross network capabilities with lost messages detection
and implementation of partitioning scheme for dynamically changing number of receivers.

The solution, even though improves a lot from where we started, leaves a lot of 
room for improvement. One area for improvement is the startup process in which all the _Akka Cluster_ 
nodes need to be started manually
with appropriate seed nodes settings, roles and such. In the follow up article
I will explore how to improve this and as a result make the solution easier
deployable to cloud infrastructure like Amazon's _AWS_.
 
## References

- Akka documentation - https://doc.akka.io/docs/akka/2.5/
- Akka in Action - https://www.manning.com/books/akka-in-action
- Akka actors basics - https://www.baeldung.com/akka-actors-java
- Reactive Streams - https://www.reactive-streams.org/
- Introduction to Reactive Programming you've been missing - https://gist.github.com/staltz/868e7e9bc2a7b8c1f754
- API comparison of RxJava2, ProjectReactor, Akka Streams - http://www.voee.tech/blog/akka-streams-rxjava2-project-reactor-api-comparison

